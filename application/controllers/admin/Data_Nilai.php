<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data_Nilai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Nilai_model', 'nilai');
    }

    public function index()
    {
        $data['title'] = 'Data nilai';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['nilai'] = $this->nilai->get_nilai();
        $data['identitas'] = $this->db->get('identitas')->row_array();

        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar');
        $this->load->view('template/top_bar', $data);
        $this->load->view('admin/data_nilai', $data);
        $this->load->view('template/footer');
    }

    public function lihat_nilai($noujian)
    {
        $data['title'] = 'Lihat nilai';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['lihat_nilai'] = $this->nilai->get_nilai_by_noujian($noujian);
        $data['siswa'] = $this->nilai->get_siswa_by_noujian($noujian);

        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar');
        $this->load->view('template/top_bar', $data);
        $this->load->view('admin/lihat_nilai', $data);
        $this->load->view('template/footer');
    }

    public function hapusnilai()
    {
        $hapus = $this->db->empty_table('nilai');
        if ($hapus) {
            echo "<div align='center'> <h3>Data Telah Terhapus Semua</h3></div>";
        } else {
            echo "<div align='center'> <h3>Data Gagal Terhapus</h3></div>";
        }
        echo "<meta http-equiv='refresh' content='2; url=./'>";
    }
}
