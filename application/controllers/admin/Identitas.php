<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Identitas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'Identitas Sekolah';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['identitas'] = $this->db->get('identitas')->row_array();

        $this->form_validation->set_rules('nama_sekolah', 'Nama Sekolah', 'required|trim');
        $this->form_validation->set_rules('npsn', 'NPSN', 'required|trim');
        $this->form_validation->set_rules('kepsek', 'Kepala Sekolah', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('template/header', $data);
            $this->load->view('template/sidebar');
            $this->load->view('template/top_bar', $data);
            $this->load->view('admin/identitas', $data);
            $this->load->view('template/footer');
        } else {

            $data = [
                'nama_sekolah' => $this->input->post('nama_sekolah'),
                'npsn' => $this->input->post('npsn'),
                'kepsek' => $this->input->post('kepsek'),
                'alamat' => $this->input->post('alamat'),
                'email' => $this->input->post('email'),
                'website' => $this->input->post('website'),
                'telpon' => $this->input->post('telpon'),
            ];

            $upload_logo = $_FILES['logo']['name'];

            if ($upload_logo) {
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['upload_path'] = './assets/img/logo/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('logo')) {
                    $new_logo = $this->upload->data('file_name');
                    $this->db->set('logo', $new_logo);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $this->db->update('identitas', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success intro-x" role="alert">Data berhasil diubah!</div>');
            redirect('admin/identitas');
        }
    }
}
