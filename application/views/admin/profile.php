<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        My Profile
    </h2>
</div>

<!-- form validation -->
<?php if (validation_errors()) : ?>
    <div class="alert alert-danger" role="alert">
        <?= validation_errors(); ?>
    </div>
<?php endif; ?>
<?php echo $this->session->flashdata('message') ?>
<!-- end form validation -->

<div class="col-span-12 lg:col-span-8 2xl:col-span-9">
    <div class="intro-y box lg:mt-5">
        <div class="flex items-center p-5 border-b border-slate-200/60 dark:border-darkmode-400">
            <h2 class="font-medium text-base mr-auto">
                Display Information
            </h2>
        </div>
        <div class="p-5">
            <div class="flex flex-col-reverse xl:flex-row flex-col">
                <div class="flex-1 mt-6 xl:mt-0">
                    <form method="POST" action="<?= site_url('admin/user/profile') ?>">
                        <div class="grid grid-cols-12 gap-x-5">
                            <div class="col-span-12 2xl:col-span-6">
                                <div>
                                    <label for="update-profile-form-1" class="form-label">Username</label>
                                    <input type="text" class="form-control" value="<?= $user['username']; ?>" disabled>
                                </div>
                                <div class="mt-3">
                                    <label for="update-profile-form-1" class="form-label">Display Name</label>
                                    <input id="name" name="name" type="text" class="form-control" placeholder="Input text" value="<?= $user['name']; ?>">
                                </div>
                            </div>
                            <div class="col-span-12 2xl:col-span-6">
                                <div class="mt-3 2xl:mt-0">
                                    <label for="update-profile-form-1" class="form-label">Email</label>
                                    <input type="text" class="form-control" placeholder="Input text" value="<?= $user['email']; ?>">
                                </div>
                                <div class="mt-3">
                                    <label for="update-profile-form-4" class="form-label">Phone Number</label>
                                    <input id="nohp" name="nohp" type="number" class="form-control" placeholder="Input no hp" value="<?= $user['nohp']; ?>">
                                </div>
                            </div>
                            <div class="col-span-12">
                                <div class="mt-3">
                                    <label for="update-profile-form-5" class="form-label">Address</label>
                                    <textarea id="alamat" name="alamat" class="form-control" placeholder="Adress"><?= $user['alamat']; ?></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary w-20 mt-3">Save</button>
                        </div>
                    </form>
                </div>
                <div class="w-52 mx-auto xl:mr-0 xl:ml-6">
                    <div class="border-2 border-dashed shadow-sm border-slate-200/60 dark:border-darkmode-400 rounded-md p-5">
                        <div class="h-40 relative image-fit cursor-pointer zoom-in mx-auto">
                            <img class="rounded-md" alt="Midone - HTML Admin Template" src="<?= base_url('assets/img/profile/') . $user['image'] ?>">
                            <input type="file" id="image" name="image" class="w-full h-full top-0 left-0 absolute opacity-0">
                            <a href="<?= site_url('admin/user/deleteimage') ?>">
                                <div title="Remove this profile photo?" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-danger right-0 top-0 -mr-2 -mt-2"> <i data-lucide="x" class="w-4 h-4"></i> </div>
                            </a>
                        </div>
                        <div class="mx-auto cursor-pointer relative mt-5">
                            <button class="btn btn-primary w-full">Change Photo</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="tab-content mt-5">
    <div id="profile" class="tab-pane active" role="tabpanel" aria-labelledby="profile-tab">
        <div class="grid grid-cols-12 gap-12">
            <div class="intro-y box col-span-12 lg:col-span-12">
                <div class="flex items-center px-5 py-5 sm:py-3 border-b border-slate-200/60 dark:border-darkmode-400">
                    <h2 class="font-medium text-base mr-auto">
                        Display Information
                    </h2>
                </div>
                <div class="flex flex-1 px-5 items-center justify-center lg:justify-start">
                    <div id="vertical-form" class="p-5">
                        <?php echo form_open_multipart('admin/user/profile'); ?>
                        <div class="preview">
                            <div>
                                <label for="vertical-form-1" class="form-label">Nama</label>
                                <input type="text" id="name" name="name" class="form-control" data-single-mode="true" value="<?= $user['name']; ?>">
                                <?= form_error('name', '<small class="text-danger" pl-3>', '</small>'); ?>
                            </div>
                            <div class="mt-3">
                                <label for="vertical-form-1" class="form-label">Username</label>
                                <input type="text" name="username" class="form-control" data-single-mode="true" value="<?= $user['username']; ?>" readonly>
                            </div>
                            <div class="mt-3">
                                <label for="vertical-form-1" class="form-label">Upload Gambar Profile</label>
                                <input class="form-control" type="file" name="image" id="image">
                            </div>
                            <button class="btn btn-primary mt-5">Save Changes</button>
                        </div>
                        </form>
                    </div>
                    <div class="w-52 mx-auto xl:mr-0 xl:ml-6">
                        <div class="border-2 border-dashed shadow-sm border-slate-200/60 dark:border-darkmode-400 rounded-md p-5">
                            <div class="h-40 relative image-fit cursor-pointer zoom-in mx-auto">
                                <img class="rounded-md" alt="Midone - HTML Admin Template" src="<?= base_url('assets/img/profile/') . $user['image'] ?>">
                                <input type="file" id="image" name="image" class="w-full h-full top-0 left-0 absolute opacity-0">
                                <a href="<?= site_url('admin/user/deleteimage') ?>">
                                    <div title="Remove this profile photo?" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-danger right-0 top-0 -mr-2 -mt-2"> <i data-lucide="x" class="w-4 h-4"></i> </div>
                                </a>
                            </div>
                            <div class="mx-auto cursor-pointer relative mt-5">
                                <button class="btn btn-primary w-full">Change Photo</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->