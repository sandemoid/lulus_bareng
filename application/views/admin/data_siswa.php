<h2 class="intro-y text-lg font-medium mt-10">
    Data Siswa
</h2>
<?php echo $this->session->flashdata('message') ?>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="<?= site_url('admin/import_siswa') ?>" class="btn btn-primary shadow-md mr-2">Import Siswa</a>
        <a href="<?= site_url('admin/data_siswa/hapussiswa') ?>" class="btn btn-danger shadow-md mr-2">Hapus Siswa</a>
        <div class="hidden md:block mx-auto text-slate-500"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-slate-500">
                <input type="text" class="form-control w-56 box pr-10" placeholder="Search...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-lucide="search"></i>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
                <tr>
                    <th class="whitespace-nowrap">NO</th>
                    <th class="whitespace-nowrap">NO UJIAN</th>
                    <th class="text-center whitespace-nowrap">NISN</th>
                    <th class="text-center whitespace-nowrap">NAME</th>
                    <th class="text-center whitespace-nowrap">JENIS KELAMIN</th>
                    <th class="text-center whitespace-nowrap">KETERANGAN</th>
                    <th class="text-center whitespace-nowrap">ACTION</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; ?>
                <?php foreach ($siswa as $s) : ?>
                    <tr class="intro-x">
                        <td class="w-40">
                            <?= $no++; ?>
                        </td>
                        <td>
                            <a href="" class="font-medium whitespace-nowrap"><?= $s['noujian'] ?></a>
                            <div class="text-slate-500 text-xs whitespace-nowrap mt-0.5"><?= $s['tgllhr'] ?></div>
                        </td>
                        <td class="text-center"><?= $s['nisn'] ?></td>
                        <td class="text-center"><?= $s['name'] ?></td>
                        <td class="text-center"><?= $s['jk'] ?></td>
                        <td class="w-40">
                            <?php if ($s['ket'] == 'lulus') : ?>
                                <div class="flex items-center justify-center text-success"> <i data-lucide="check-square" class="w-4 h-4 mr-2"></i> Lulus </div>
                            <?php else : ?>
                                <div class="flex items-center justify-center text-danger"> <i data-lucide="check-square" class="w-4 h-4 mr-2"></i> Tidak Lulus </div>
                            <?php endif ?>
                        </td>
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center text-danger" href="javascript:;" data-tw-toggle="modal" data-tw-target="#delete-confirmation-modal<?= $s['id'] ?>"> <i data-lucide="trash-2" class="w-4 h-4 mr-1"></i> Delete </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- END: Data List -->

    <?php $this->load->view('template/pagination'); ?>

</div>
<!-- BEGIN: Delete Confirmation Modal -->
<?php foreach ($siswa as $s) : ?>
    <div id="delete-confirmation-modal<?= $s['id'] ?>" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Are you sure?</div>
                        <div class="text-slate-500 mt-2">
                            Do you really want to delete these records?
                            <br>
                            This process cannot be undone.
                        </div>
                    </div>
                    <div class="px-5 pb-8 text-center">
                        <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                        <a class="btn btn-danger w-24" href="<?= site_url('admin/data_siswa/deletesiswa/') . $s['id']; ?>">Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<!-- END: Delete Confirmation Modal -->