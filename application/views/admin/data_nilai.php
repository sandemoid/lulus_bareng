<h2 class="intro-y text-lg font-medium mt-10">
    Data Nilai
</h2>
<?php echo $this->session->flashdata('message') ?>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="" class="btn btn-primary shadow-md mr-2">Import Nilai</a>
        <a href="<?= site_url('admin/data_nilai/hapusnilai') ?>" class="btn btn-danger shadow-md mr-2">Hapus Nilai</a>
        <div class="hidden md:block mx-auto text-slate-500"></div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-slate-500">
                <input type="text" class="form-control w-56 box pr-10" placeholder="Search...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-lucide="search"></i>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
                <tr>
                    <th class="whitespace-nowrap">NO</th>
                    <th class="whitespace-nowrap">NO UJIAN</th>
                    <th class="text-center whitespace-nowrap">NAMA</th>
                    <th class="text-center whitespace-nowrap">NILAI</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; ?>
                <?php foreach ($nilai as $n) : ?>
                    <tr class="intro-x">
                        <td class="w-40">
                            <?= $no++; ?>
                        </td>
                        <td><?= $n['noujian'] ?></td>
                        <td class="text-center"><?= $n['name'] ?></td>
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center text-danger" href="<?= site_url('admin/data_nilai/lihat_nilai/' . $n['noujian']) ?>"> <i data-lucide="eye" class="w-4 h-4 mr-1"></i> Lihat Nilai </a>
                            </div>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- END: Data List -->

    <?php $this->load->view('template/pagination'); ?>

</div>