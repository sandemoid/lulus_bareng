<?php $this->load->view('template/sidebar_mobile') ?>
<!-- END: Mobile Menu -->
<div class="flex mt-[4.7rem] md:mt-0">
    <!-- BEGIN: Side Menu -->
    <nav class="side-nav">
        <a href="" class="intro-x flex items-center pl-5 pt-4">
            <img alt="Midone - HTML Admin Template" class="w-6" src="<?= base_url('assets') ?>/dist/images/logo.svg">
            <span class="hidden xl:block text-white text-lg ml-3"> Lulus Bareng </span>
        </a>
        <div class="side-nav__devider my-6"></div>
        <ul>
            <li>
                <a href="<?= site_url('admin/home') ?>" class="side-menu <?= active_menu('home') ?>">
                    <div class="side-menu__icon"> <i data-lucide="home"></i> </div>
                    <div class="side-menu__title">
                        Dashboard
                    </div>
                </a>
            </li>
            <li>
                <a href="<?= site_url('admin/identitas') ?>" class="side-menu <?= active_menu('identitas') ?>">
                    <div class="side-menu__icon"> <i data-lucide="settings"></i> </div>
                    <div class="side-menu__title">
                        Identitas Sekolah
                    </div>
                </a>
            </li>
            <li>
                <a href="javascript:;" class="side-menu <?= active_menu('Data_Siswa') ?>">
                    <div class="side-menu__icon"> <i data-lucide="folder"></i> </div>
                    <div class="side-menu__title">
                        Master Data
                        <div class="side-menu__sub-icon <?= rotate_active('Data_Siswa') ?>"> <i data-lucide="chevron-down"></i> </div>
                    </div>
                </a>
                <ul class="menu__sub-open <?= dropdown_active('Data_Siswa') ?>">
                    <li>
                        <a href="<?= site_url('admin/Data_Siswa') ?>" class="side-menu <?= active_menu('Data_Siswa') ?>">
                            <div class="side-menu__icon"> <i data-lucide="activity"></i> </div>
                            <div class="side-menu__title"> Data Siswa </div>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('admin/Data_Nilai') ?>" class="side-menu <?= active_menu('Data_Nilai') ?>">
                            <div class="side-menu__icon"> <i data-lucide="activity"></i> </div>
                            <div class="side-menu__title"> Data Nilai </div>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;" class="side-menu <?= active_menu('Import_Siswa') ?>">
                    <div class="side-menu__icon"> <i data-lucide="hard-drive"></i> </div>
                    <div class="side-menu__title">
                        Import Data
                        <div class="side-menu__sub-icon <?= rotate_active('Import_Siswa') ?>"> <i data-lucide="chevron-down"></i> </div>
                    </div>
                </a>
                <ul class="menu__sub-open <?= dropdown_active('Import_Siswa') ?>">
                    <li>
                        <a href="<?= site_url('admin/Import_Siswa') ?>" class="side-menu <?= active_menu('Import_Siswa') ?>">
                            <div class="side-menu__icon"> <i data-lucide="activity"></i> </div>
                            <div class="side-menu__title"> Import Siswa </div>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('admin/Import_Nilai') ?>" class="side-menu <?= active_menu('Import_Nilai') ?>">
                            <div class="side-menu__icon"> <i data-lucide="activity"></i> </div>
                            <div class="side-menu__title"> Import Nilai </div>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="side-nav__devider my-6"></li>
            <li>
                <a href="<?= site_url('admin/user') ?>" class="side-menu <?= active_menu('user') ?>">
                    <div class="side-menu__icon"> <i data-lucide="users"></i> </div>
                    <div class="side-menu__title">
                        User
                    </div>
                </a>
            </li>
            <li>
                <a href="<?= site_url('admin/jadwal') ?>" class="side-menu <?= active_menu('pesan') ?>">
                    <div class="side-menu__icon"> <i data-lucide="calendar"></i> </div>
                    <div class="side-menu__title">
                        Jadwal
                    </div>
                </a>
            </li>
            <li>
                <a href="<?= site_url('admin/setting') ?>" class="side-menu <?= active_menu('setting') ?>">
                    <div class="side-menu__icon"> <i data-lucide="file-text"></i> </div>
                    <div class="side-menu__title">
                        Setting Suket
                    </div>
                </a>
            </li>
            <li class="side-nav__devider my-6"></li>
            <li>
                <a href="<?= site_url('admin/auth/logout') ?>" class="side-menu">
                    <div class="side-menu__icon"> <i data-lucide="log-out"></i> </div>
                    <div class="side-menu__title">
                        Logout
                    </div>
                </a>
            </li>
        </ul>
    </nav>
    <!-- END: Side Menu -->