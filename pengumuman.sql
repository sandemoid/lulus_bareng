-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2022 at 09:07 AM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pengumuman`
--

-- --------------------------------------------------------

--
-- Table structure for table `hubungi`
--

CREATE TABLE `hubungi` (
  `id_hubungi` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `identitas`
--

CREATE TABLE `identitas` (
  `id` int(11) NOT NULL,
  `nama_sekolah` varchar(128) NOT NULL,
  `npsn` varchar(128) NOT NULL,
  `kepsek` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `alamat` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `website` varchar(128) NOT NULL,
  `telpon` varchar(50) NOT NULL,
  `logo` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `identitas`
--

INSERT INTO `identitas` (`id`, `nama_sekolah`, `npsn`, `kepsek`, `nip`, `alamat`, `email`, `website`, `telpon`, `logo`) VALUES
(1, 'SMA Negeri 1 Gelumbang', '38424789', 'Sandi Maulidika', '-', 'Jalan Palembang-Prabumulih Kilometer 58 Gelumbang, Muara Enim', 'smansagel@gmail.com', 'smansagel.sch.id', '087801751653', '8e4294664661d6f46dcd83278a8210fb.png');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id` int(11) NOT NULL,
  `waktu` datetime NOT NULL,
  `tahun_ajaran` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id`, `waktu`, `tahun_ajaran`) VALUES
(1, '2022-11-07 07:39:00', '2022/2023');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL,
  `noujian` varchar(200) NOT NULL,
  `nama_mapel` varchar(200) NOT NULL,
  `nilai_sekolah` varchar(200) NOT NULL,
  `nilai_un` varchar(200) NOT NULL,
  `nilai_akhir` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `noujian`, `nama_mapel`, `nilai_sekolah`, `nilai_un`, `nilai_akhir`) VALUES
(37, '201-210-015', 'Bahasa Indonesia', '76', '75', '43'),
(38, '201-210-015', 'Kimia', '54', '54', '56'),
(39, '201-210-015', 'Matematika', '76', '75', '56'),
(40, '201-210-015', 'Biologi', '34', '', ''),
(41, '201-210-015', 'Bahasa Inggris', '45', '', ''),
(42, '201-210-015', 'Fisika', '76', '', ''),
(43, '201-210-015', 'PKn', '43', '', ''),
(44, '201-210-016', 'Bahasa Indonesia', '43', '54', '54');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `kop1` varchar(128) NOT NULL,
  `kop2` varchar(128) NOT NULL,
  `kop3` varchar(128) NOT NULL,
  `alamat` varchar(128) NOT NULL,
  `nomor_sk` varchar(128) NOT NULL,
  `nomor_sk2` varchar(50) NOT NULL,
  `nomor_sk3` year(4) NOT NULL,
  `tanggal_sk` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `kop1`, `kop2`, `kop3`, `alamat`, `nomor_sk`, `nomor_sk2`, `nomor_sk3`, `tanggal_sk`) VALUES
(1, 'PEMERINTAH PROVINSI SUMATERA SELATAN', 'DINAS PENDIDIKAN DAN KEBUDAYAAN', 'SMA NEGERI 1 GELUMBANG', 'Gelumbang', '421.3', '161/SMAN1GLB/422', 2022, '2022-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `noujian` varchar(200) NOT NULL,
  `nisn` varchar(128) NOT NULL,
  `name` varchar(200) NOT NULL,
  `jk` varchar(50) NOT NULL,
  `tgllhr` varchar(128) NOT NULL,
  `tmptlhr` varchar(100) NOT NULL,
  `sekolah` varchar(200) NOT NULL,
  `jurusan` varchar(200) NOT NULL,
  `ket` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `noujian`, `nisn`, `name`, `jk`, `tgllhr`, `tmptlhr`, `sekolah`, `jurusan`, `ket`) VALUES
(134, '201-210-015', '3219853', 'Arjun', 'Laki - laki', '06 december 2005', 'Gelumbang', 'SMA 1', 'IPA', 'lulus'),
(135, '201-210-016', '3219854', 'Arjun', 'Laki - laki', '7 juni 2005', 'Gelumbang', 'SMA 1', 'IPA', 'lulus'),
(136, '201-210-017', '3219855', 'Arjun', 'Laki - laki', '8 juni 2005', 'Gelumbang', 'SMA 1', 'IPA', 'lulus'),
(137, '201-210-018', '3219856', 'Arjun', 'Laki - laki', '9 juni 2005', 'Gelumbang', 'SMA 1', 'IPA', 'lulus'),
(138, '201-210-019', '3219857', 'Arjun', 'Laki - laki', '10 juni 2005', 'Gelumbang', 'SMA 1', 'IPA', 'lulus'),
(139, '201-210-020', '3219858', 'Arjun', 'Laki - laki', '11 juni 2005', 'Gelumbang', 'SMA 1', 'IPA', 'lulus'),
(140, '201-210-021', '3219859', 'Arjun', 'Perempuan', '12 juni 2005', 'Gelumbang', 'SMA 1', 'IPS', 'tidak lulus'),
(141, '201-210-022', '3219860', 'Arjun', 'Perempuan', '13 juni 2005', 'Gelumbang', 'SMA 1', 'IPS', 'tidak lulus'),
(142, '201-210-023', '3219861', 'Arjun', 'Perempuan', '14 juni 2005', 'Gelumbang', 'SMA 1', 'IPS', 'tidak lulus'),
(143, '201-210-024', '3219862', 'Arjun', 'Perempuan', '15 juni 2005', 'Gelumbang', 'SMA 1', 'IPS', 'tidak lulus'),
(144, '201-210-025', '3219863', 'Arjun', 'Perempuan', '16 juni 2005', 'Gelumbang', 'SMA 1', 'IPS', 'tidak lulus'),
(145, '201-210-026', '3219864', 'Arjun', 'Perempuan', '17 juni 2005', 'Gelumbang', 'SMA 1', 'IPS', 'tidak lulus');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `nohp` varchar(50) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `name`, `alamat`, `nohp`, `email`, `image`) VALUES
(6, 'admin', '$2y$10$S6UmuLH.Cb0jT3A2ivLoOupGyMFuY08DNgXkxwyX7R9gVrieOiwbC', 'Administrator', 'Jl. Pipa Gelumbang', '087801751656', 'admin@example.com', 'sandi.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hubungi`
--
ALTER TABLE `hubungi`
  ADD PRIMARY KEY (`id_hubungi`);

--
-- Indexes for table `identitas`
--
ALTER TABLE `identitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `no` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hubungi`
--
ALTER TABLE `hubungi`
  MODIFY `id_hubungi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `identitas`
--
ALTER TABLE `identitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
